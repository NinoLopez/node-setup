#!/bin/bash

#setup brew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

#install nvm
brew install nvm

#install stable node
nvm install stable
nvm run stable
nvm alias default stable
	
#install gulp
sudo npm install -g gulp
